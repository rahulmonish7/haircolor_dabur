
import socket
import sys
import cv2
import pickle
import numpy as np
import struct ## new
import zlib
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
import time

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
#cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128   # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 3 
cfg.MODEL.WEIGHTS = "../model_0003999.pth"

cfg.MODEL.DEVICE='cpu'
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8   # set the testing threshold for this model
predictor = DefaultPredictor(cfg)



HOST='0.0.0.0'
PORT=5050

print(socket.gethostname())
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print('Socket created')

s.bind((HOST, 8000))
print('Socket bind complete')
s.listen(5)
print('Socket now listening')
#print("hostname: ",s.gethostbyname(socket.gethostname()))

conn,addr=s.accept()

data = b""
count=0
payload_size = struct.calcsize(">L")
print(payload_size)
print("payload_size: {}".format(payload_size))
while True:
    while len(data) < payload_size:
        #print("Recv: {}".format(len(data)))
        data += conn.recv(4096)

    print("Done Recv: {}".format(count))
    count+=1
    packed_msg_size = data[:payload_size]
    data = data[payload_size:]
    msg_size = struct.unpack(">L", packed_msg_size)[0]
    #print("msg_size: {}".format(msg_size))
    while len(data) < msg_size:
        data += conn.recv(4096)
    frame_data = data[:msg_size]
    data = data[msg_size:]

    frame=pickle.loads(frame_data, fix_imports=True, encoding="bytes")
    frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
    #print("size of frame:", frame.shape)
    #outputs= predictor(frame)
    #cv2.imshow('ImageWindow',frame)

    cv2.waitKey(1)
