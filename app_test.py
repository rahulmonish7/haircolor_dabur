

# Using flask to make an api 
# import necessary libraries and functions 
from flask import Flask, jsonify, request 
# You may need to restart your runtime prior to this, to let your installation take effect
# Some basic setup:
# Setup detectron2 logger
# import detectron2
# from detectron2.utils.logger import setup_logger
# setup_logger()

# # import some common libraries
import numpy as np
from process import apply_model
import cv2
import skimage.io
# import random
# #from google.colab.patches import cv2_imshow

# # import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
# from detectron2.utils.visualizer import Visualizer
# from detectron2.data import MetadataCatalog
# from detectron2.structures import masks, boxes
# from detectron2.engine import DefaultTrainer
# from detectron2.modeling import build_model
import glob, os
from process import apply_model

  
# creating a Flask app 
app = Flask(__name__) 

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
#cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128   # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 2 
cfg.MODEL.WEIGHTS = "../model_0000999.pth"

cfg.MODEL.DEVICE='cpu'
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.6   # set the testing threshold for this model
predictor = DefaultPredictor(cfg)

# on the terminal type: curl http://127.0.0.1:5000/ 
# returns hello world when we use GET. 
# returns the data that we send when we use POST. 
@app.route('/', methods = ['GET', 'POST']) 
def home(): 
    if(request.method == 'GET'): 
  
        data = "hello world"
        return jsonify({'data': data}) 
    
    elif(request.method == 'POST'):

        image = request.files.get('image')
        print(type(image))
        image = skimage.io.imread(image)
        image3  = np.array(image)

        # response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0])
        #         }
        
        response= apply_model(image3, predictor)
        

        return response
  
  
# A simple function to calculate the square of a number 
# the number to be squared is sent in the URL when we use GET 
# on the terminal type: curl http://127.0.0.1:5000 / home / 10 
# this returns 100 (square of 10) 
@app.route('/home/<int:num>', methods = ['GET']) 
def disp(num): 
  
    return jsonify({'data': num**2}) 
  
  
# driver function 
if __name__ == '__main__': 
  
    app.run('0.0.0.0',debug = True)
    
