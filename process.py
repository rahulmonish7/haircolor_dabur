from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
from detectron2.structures import masks, boxes
from detectron2.engine import DefaultTrainer
from detectron2.modeling import build_model
import glob, os
import detectron2
import matplotlib.pyplot as plt
import torch
import numpy as np
import cv2
import matplotlib.cm as cm
import base64



class_dict={'Hair':0, 'Face':1 }


def apply_model(file, predictor):
    predictor= predictor
    im = file
    outputs= predictor(im)

    instances= outputs["instances"]
    mask= instances.pred_masks
    classes= instances.pred_classes
    print(classes)

    array= convert_to_bw(instances)
    save_image(array)
    with open("filename.png", "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read())
    return encoded_string

def convert_to_bw(instances):
    image_masks= instances.pred_masks
    classes= instances.pred_classes
    try:
        arr= torch.zeros(image_masks[0].shape)
    except:
        print("not detected")
    for category in range(len(classes)):
        if classes[category]== class_dict['Hair']:
            array= torch.Tensor.cpu(image_masks[category]).detach().numpy()
            arr+= array
            arr= np.where((arr!=0) & (arr!=1), 1, arr)            

    return arr

def save_image(image):
    plt.imsave('filename.png', np.array(image), cmap=cm.gray)
    

