from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog
from detectron2.structures import masks, boxes
from detectron2.engine import DefaultTrainer
from detectron2.modeling import build_model
import glob, os
import detectron2
from detectron2.utils.logger import setup_logger
import matplotlib.pyplot as plt
import torch
import numpy as np
import cv2
import matplotlib.cm as cm


cfg = get_cfg()
#model = build_model(cfg)
#cfg.DATASETS.TRAIN = ("PIM2_train",) 
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_101_FPN_3x.yaml"))
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128   # faster, and good enough for this toy dataset (default: 512)
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 6 
#cfg.MODEL.WEIGHTS = os.path.join(cfg.OUTPUT_DIR, "drive/My Drive/PIM/detectron2/output/model_final.pth")
#cfg.MODEL.WEIGHTS= model.load_state_dict(torch.load("output/model_final.pth", map_location='cpu'))
#cfg.MODEL.WEIGHTS = "drive/My Drive/output/new/model_final.pth"
cfg.MODEL.WEIGHTS = "model_0001999.pth"

#cfg.MODEL.DEVICE='cpu'
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7   # set the testing threshold for this model

#cfg.DATASETS.TEST = ("chumma_val", )
predictor = DefaultPredictor(cfg)





im = cv2.imread(file)
outputs= predictor(im)
v = Visualizer(im[:, :, ::-1], MetadataCatalog.get("PIM2_train"), scale=1.2)
out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
result= out.get_image()[:, :, ::-1]
result2= result.copy()
p= masks.BitMasks(result2)
instances= outputs["instances"]
a= instances.pred_masks
cv2_imshow(out.get_image()[:, :, ::-1])
print(instances.pred_boxes)



array= torch.Tensor.cpu(a[0]).detach().numpy()


import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
plt.imsave('filename.png', np.array(array), cmap=cm.gray)