from __future__ import print_function
import requests
import json
import cv2
import base64
addr = 'http://13.233.21.63:5000'
test_url = addr 
# prepare headers for http request
content_type = 'image/jpeg'
headers = {'content-type': content_type}
img = cv2.imread(r'g1.jpg')
# encode image as jpeg
_, img_encoded = cv2.imencode('.jpg', img)
# send http request with image and receive response
response = requests.post(test_url, data=img_encoded.tostring(), headers=headers)
# decode response
print(response.text)


# from PIL import Image
# from base64 import decodestring

# image = Image.fromstring('RGB',(100,100),decodestring(response.text))
# image.save("foo.png")